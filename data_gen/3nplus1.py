with open("data.tsv", "w") as out_file:
    for i in range(1, 2001):
        if i % 2 == 0:
            print(i, i // 2, 1, sep="\t", file=out_file)
        if i > 4 and i % 6 == 4:
            print(i // 3, i, 1, sep="\t", file=out_file)
